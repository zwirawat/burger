import material.Material;
import material.Material_q;
import material.Material_quantity;


import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class OrderImpl implements OrderRepo {
    Scanner fr = new Scanner(System.in);
    Material_q material = new Material_quantity();

    Map<String, Float> order = new HashMap<String, Float>();
    Map<String, Float> orderUs = new HashMap<String, Float>();


    int[] raw = new int[6];
    String menu[] = {"1.Chicken Burger", "2.Beef Burger", "3.Chicken hotdog", "4.Beef hotdog", "5.Checkout"};

    @Override
    public void display() {
        try {
            System.out.print("Functions:\n" +
                    "1. Order\n" +
                    "2. End the day\n");
            System.out.print("Please choose the function (1/2): ");
            int selected = fr.nextInt();
            switch (selected) {

                case 1: {

                    for (int i = 0; i < 1; i--) {
                        System.out.print("Ordering Menu:\n");
                        for (int j = 0; j < menu.length; j++) {
                            System.out.println(menu[j]);
                        }
                        System.out.print("Please choose your option (1 - 5): ");
                        int select = fr.nextInt();
                        if (select == 1) {
                            System.out.print("How many Chicken burger?: ");
                            int chk = fr.nextInt();
                            float resp = (float) ((float) chk * 6.50);
                            order.put("Chicken burger", resp);
                            orderUs.put("Chicken burger", resp);
                            Map<String, Integer> item = material.materialMenu();
                            int result1 = item.get("Chicken patty") - chk;
                            int result2 = item.get("Round bun") - chk;
                            item.replace("Chicken patty", result1);
                            item.replace("Round bun", result2);


                        } else if (select == 2) {
                            System.out.print("How many Beef Burger?: ");
                            int chk = fr.nextInt();
                            float resp = (float) ((float) chk * 6.50);
                            order.put("Beef Burger", resp);
                            orderUs.put("Beef Burger", resp);
                            Map<String, Integer> item = material.materialMenu();
                            int result1 = item.get("Beef patty") - chk;
                            int result2 = item.get("Round bun") - chk;
                            item.replace("Beef patty", result1);
                            item.replace("Round bun", result2);

                        } else if (select == 3) {
                            System.out.print("How many Chicken hotdog?: ");
                            int chk = fr.nextInt();
                            float resp = (float) ((float) chk * 6.50);
                            order.put("Chicken hotdog", resp);
                            orderUs.put("Chicken hotdog", resp);

                            Map<String, Integer> item = material.materialMenu();
                            int result1 = item.get("Chicken sausage") - chk;
                            int result2 = item.get("Long bun") - chk;
                            item.replace("Chicken sausage", result1);
                            item.replace("Long bun", result2);

                        } else if (select == 4) {
                            System.out.print("How many Beef hotdog?: ");
                            int chk = fr.nextInt();
                            float resp = (float) ((float) chk * 6.50);
                            order.put("Beef hotdog", resp);
                            orderUs.put("Beef hotdog", resp);
                            Map<String, Integer> item = material.materialMenu();
                            int result1 = item.get("Beef sausage") - chk;
                            int result2 = item.get("Long bun") - chk;
                            item.replace("Beef sausage", result1);
                            item.replace("Long bun", result2);

                        } else if (select == 5) {
                            System.out.println("Receipt");
                            System.out.println("---------------------------------");
                            System.out.println(new Date());
                            System.out.println("Item: " +orderUs);
                            float sum = (float) orderUs.values().stream().mapToDouble(j->j).sum();
                            System.out.println("total: "+sum);


                            // remove orderUs
                            order.keySet().forEach(orderUs::remove);
                            System.out.println("loginfo :"+orderUs);
                            break;

                        } else {
                            System.out.print("Wrong option, please enter a valid option (1-5)");
                        }
                    }

                    break;
                }
                case 2: {

                    System.out.println("1. Enter the raw material quantity:\n" +
                            "2. Order and material quantity");
                    System.out.print("Please choose the function (1/2): ");

                    int pickOrder = fr.nextInt();
                    switch (pickOrder) {
                        case 1: {
                            material.materialQuantity(raw);
                            break;
                        }
                        case 2: {
                            Map<String, Integer> item = material.materialMenu();
                            System.out.println("item Quantity sold : " + order);
                            float sum = (float) order.values().stream().mapToDouble(j->j).sum();
                            System.out.println("Subtotal (RM) : "+ sum);
                            System.out.println("-------------------------------");
                            System.out.println("Raw material left:");
                            System.out.println("Item :" + item);
                        }

                    }
                    break;
                }

            }
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }
}
