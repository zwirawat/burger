package material;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Material_quantity implements Material_q {
    Map<String, Integer> item = new HashMap<String, Integer>();
    Material material = new Material(item);
    String materialStr[] = {"Chicken patty", "Beef patty", "Chicken sausage", "Beef sausage", "Round bun", "Long bun"};

    @Override
    public Map<String, Integer> materialQuantity(int[] materials) {
        Scanner fr = new Scanner(System.in);
        System.out.println("Enter the raw material quantity");
        for (int i = 0; i < materials.length; i++) {
            System.out.print(materialStr[i] + ": ");
            materials[i] = fr.nextInt();
            item.put(materialStr[i], materials[i]);
        }
        material.setItem(item);
        return item;
    }

    @Override
    public Map<String, Integer> materialMenu() {
        return material.getItem();
    }
}
