package material;

import java.util.Map;

public interface Material_q {

    public Map<String, Integer> materialQuantity(int[] materials);
    public Map<String, Integer> materialMenu();
}
