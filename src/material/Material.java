package material;

import java.util.HashMap;
import java.util.Map;

public class Material {

    private Map<String, Integer> item = new HashMap<String, Integer>();

    public Map<String, Integer> getItem() {
        return item;
    }

    public void setItem(Map<String, Integer> item) {
        this.item = item;
    }

    public Material(Map<String, Integer> item) {
        this.item = item;
    }
}



